

// faq 

const $faqBlock = document.querySelector('.faq-column');

if ($faqBlock) {
    $faqBlock.addEventListener('click', e => {
        e.preventDefault();
        const $target = e.target;
        const $faqItem = $target.closest('.faq-column .item');
        const $faqItemTop = $target.closest('.item-title');
        const $faqItemAll = $faqBlock.querySelectorAll('.faq-column .item');
    
        if ($faqItem) {
            if ($faqItem.getAttribute('active') && $faqItemTop) {
    
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.classList.remove('active');
                $faqItem.removeAttribute('active');
            } else {
                
                $faqItemAll.forEach(item => {
                    item.classList.remove('active');
                    item.removeAttribute('active');
                });
                $faqItem.setAttribute('active', 'active');
                $faqItem.classList.add('active');
               
            }
            
        } 
    
    });
    
}

// menu

const v_headerMenuM = document.querySelector('.header-menu-m'),
v_modalMenu = document.querySelector('.modal-menu');

if (v_headerMenuM) {
    
    v_headerMenuM.addEventListener('click', () => {
        
        if (v_headerMenuM.getAttribute('active')) {
            v_headerMenuM.classList.remove('active');
            v_headerMenuM.removeAttribute('active');
            v_modalMenu.classList.remove('active');
        } else {
         
            v_headerMenuM.classList.add('active');
            v_headerMenuM.setAttribute('active', 'active');
            v_modalMenu.classList.add('active');
        }
    });

}


// показать весь текст

const $boxVisDescp = document.querySelectorAll('.box-vis-descp');

if ($boxVisDescp) {

    $boxVisDescp.forEach(item => {
        const $read = item.querySelector('.read-more');
        const $subtext = item.querySelectorAll('.subtitle');
 
        $subtext.forEach(item => {
            item.style.display = 'none';
            $subtext[0].style.display = '';
        })
        if ($read) {
            $read.addEventListener('click', e => {
               
                e.preventDefault();
                $subtext[0].style.display = '';
                $subtext.forEach(item => {
             
                    if (item.getAttribute('active')) {
    
                        item.style.display = 'none';
                        $subtext[0].style.display = '';
                        item.removeAttribute('active');
                        item.classList.remove('active');
    
                    } else {
                        item.style.display = '';
                        
                        item.classList.add('active');
                        item.setAttribute('active', 'active');
    
                    }
                })
            });
        }
      
    });
}



const v_partners = document.querySelector('.partners');

if (v_partners) {

    const swiper = new Swiper('.swiper-container', {
        // Default parameters
        slidesPerView: 1,
        spaceBetween: 0,
        // Responsive breakpoints
        breakpoints: {
          // when window width is >= 320px
          300: {
            slidesPerView: 3,
            spaceBetween: 0
          },
          330: {
            slidesPerView: 3.5,
            spaceBetween: 0
          },
          // when window width is >= 640px
          980: {
            slidesPerView: 4,
            spaceBetween: 0
          },
          1000: {
            slidesPerView: 12,
            spaceBetween: 0
          }
        }
    })
}